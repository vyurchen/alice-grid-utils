#ifndef ROOT_TAliceCollection
#define ROOT_TAliceCollection

#include "TGridCollection.h"

class TAliceCollection : public TGridCollection {
public:
    virtual void        AddFast(TGridCollection *addcollection) = 0;
    // static TGridCollection *Open(const char *collectionurl,
    //                              UInt_t maxentries = 1000000) = 0;
    // static TGridCollection *OpenQuery(TGridResult * queryresult,
    //                                   Bool_t nogrouping = kFALSE) = 0;
    // static TAliceCollection *OpenAlienCollection(TGridResult * queryresult,
    //                                              Option_t* option = "") = 0;

    // const char *GetOutputFileName(const char *infile, Bool_t rename = kTRUE,
    //                               const char *suffix="root") = 0;
};

#endif
