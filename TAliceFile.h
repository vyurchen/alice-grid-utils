#ifndef ROOT_TAliceFile
#define ROOT_TAliceFile

#include <TNetXNGFile.h>
#include <TXNetFile.h>

class TAliceFile : public TNetXNGFile {
public:
  TAliceFile() : TNetXNGFile() {}
  TAliceFile(const char *purl, Option_t *option = "",
             const char *ftitle = "", Int_t compress = 1,
             Bool_t parallelopen = kFALSE, const char *lurl = 0,
             const char *authz = 0) :
    TNetXNGFile(purl, lurl, option, ftitle, compress, 0, parallelopen) { }

  virtual ~TAliceFile() {};

  virtual const char  *GetGUID() const  = 0;
  virtual Double_t     GetElapsed() const = 0;
  virtual Int_t        GetImage() const = 0;
  virtual const char  *GetLfn() const   = 0;
  virtual Int_t        GetNreplicas() const = 0;
  virtual Long64_t     GetOpenTime() const = 0;
  virtual const char  *GetPfn() const   = 0;
  virtual const char  *GetSE() const    = 0;
  virtual const char  *GetUrl() const   = 0;

protected:
  virtual void         SetGUID(const char *guid) = 0;
  virtual void         SetElapsed(Double_t real) = 0;
  virtual void         SetNreplicas(Int_t nrep) = 0;
  virtual void         SetPfn(const char *pfn) = 0;
  virtual void         SetSE(const char *se)   = 0;
  virtual void         SetPreferredSE(const char *prefse) {};
  virtual void         SetUrl(const char *url) = 0;
};

#endif
